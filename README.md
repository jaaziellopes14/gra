# Golden Raspberry Awards

Indicados e vencedores da categoria *Pior Filme* do Golden Raspberry Awards.

**O sistema tem por objetivo disponibilizar a consulta dos Produtores com maior e menor intervalo entre duas premiações consecutivas.**

## Iniciar rapidamente

Siga as instruções abaixo para executar o projeto localmente.

### Requisitos

Para compilar o projeto você precisará do JDK 8 e do Maven (versão 3.6.3) instalados.

### Informações do projeto

Este projeto utiliza Spring Boot, Maven, Banco de dados H2 (em memória) e APIs RESTful para ler de um arquivo CSV uma lista dos piores filmes do Golden Raspberry Awards.

### Executar o projeto

Para executar o projeto, basta rodar o seguinte comando:
```
mvn spring-boot:run
```

### Acessar banco de dados localmente
    
Link: http://localhost:8080/h2-console
    
Dados de conexão:
```
url=jdbc:h2:mem:gra
username=sa
password=
```

## Rodar os testes de integração

Rodar o seguinte comando:
```
mvn verify -Pfailsafe
```

## APIs disponibilizadas

Retorna todas as premiações em ordem alfabética e temporal.
```
http://localhost:8080/api/producers/winners
```

Retorna os Produtores com maior e menor intervalo entre duas premiações consecutivas.
```
http://localhost:8080/api/producers/top-bottom-interval-winner
```

## Autor

**Jaaziel Lopes (jaaziellopes14)**
