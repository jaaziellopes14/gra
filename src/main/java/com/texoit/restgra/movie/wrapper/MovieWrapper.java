package com.texoit.restgra.movie.wrapper;

import com.texoit.restgra.entreprise.CsvMovie;
import com.texoit.restgra.movie.model.Movie;
import com.texoit.restgra.producer.model.Producer;
import com.texoit.restgra.producer.representation.ProducerCreateIntent;
import com.texoit.restgra.studio.model.Studio;
import com.texoit.restgra.studio.wrapper.StudioWrapper;

import java.util.List;
import java.util.stream.Collectors;

public class MovieWrapper {

    private MovieWrapper() {
    }

    public static List<Movie> toEntity(List<CsvMovie> csvMovieList, List<Studio> studios, List<Producer> producers) {
        return csvMovieList.stream().map(csvMovie ->
                toEntity(studios, producers, csvMovie)).collect(Collectors.toList());
    }

    private static Movie toEntity(List<Studio> studios, List<Producer> producers, CsvMovie csvMovie) {
        return Movie.builder()
                .title(csvMovie.getTitle())
                .year(csvMovie.getYear())
                .winner("yes".equals(csvMovie.getWinner()))
                .studios(findStudios(studios, csvMovie))
                .producers(findProducers(producers, csvMovie))
                .build();
    }

    private static List<Producer> findProducers(List<Producer> producers, CsvMovie csvMovie) {
        return ProducerCreateIntent.stringToArray(csvMovie.getProducers())
                .map(producerName -> producers.stream()
                .filter(producer -> producer.getName().equals(producerName)).findFirst().get())
                .collect(Collectors.toList());
    }

    private static List<Studio> findStudios(List<Studio> studios, CsvMovie csvMovie) {
        return StudioWrapper.stringToArray(csvMovie.getStudios())
            .map(studioName -> studios.stream()
            .filter(studio -> studio.getName().equals(studioName)).findFirst().get())
            .collect(Collectors.toList());
    }

}
