package com.texoit.restgra.movie.model;

import com.texoit.restgra.producer.model.Producer;
import com.texoit.restgra.studio.model.Studio;
import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "movie")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder(toBuilder = true)
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    private Long id;
    private String title;
    private Integer year;
    @Type(type = "numeric_boolean")
    private boolean winner;

    @Lazy
    @ManyToMany
    private List<Studio> studios;

    @Lazy
    @ManyToMany
    private List<Producer> producers;

}
