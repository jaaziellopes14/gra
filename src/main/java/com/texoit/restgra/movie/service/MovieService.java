package com.texoit.restgra.movie.service;

import com.texoit.restgra.movie.model.Movie;
import com.texoit.restgra.movie.repository.MovieRepository;
import org.springframework.stereotype.Service;

@Service
public class MovieService {

    private final MovieRepository repository;

    public MovieService(MovieRepository repository) {
        this.repository = repository;
    }

    public Movie create(Movie toCreate) {
        return repository.save(toCreate);
    }

}
