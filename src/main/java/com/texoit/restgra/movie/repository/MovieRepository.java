package com.texoit.restgra.movie.repository;

import com.texoit.restgra.movie.model.Movie;
import org.springframework.data.repository.CrudRepository;

public interface MovieRepository extends CrudRepository<Movie, Long> {
}
