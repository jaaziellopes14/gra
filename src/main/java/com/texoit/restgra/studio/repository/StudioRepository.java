package com.texoit.restgra.studio.repository;

import com.texoit.restgra.studio.model.Studio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudioRepository extends JpaRepository<Studio, Long> {
}
