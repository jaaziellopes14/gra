package com.texoit.restgra.studio.service;

import com.texoit.restgra.studio.model.Studio;
import com.texoit.restgra.studio.repository.StudioRepository;
import org.springframework.stereotype.Service;

@Service
public class StudioService {

    private final StudioRepository repository;

    public StudioService(StudioRepository repository) {
        this.repository = repository;
    }

    public Studio create(Studio toCreate) {
        return repository.save(toCreate);
    }

}
