package com.texoit.restgra.studio.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "studio")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder(toBuilder = true)
public class Studio {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

}
