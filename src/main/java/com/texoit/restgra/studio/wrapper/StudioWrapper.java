package com.texoit.restgra.studio.wrapper;

import com.texoit.restgra.entreprise.CsvMovie;
import com.texoit.restgra.studio.model.Studio;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudioWrapper {

    private StudioWrapper() {
    }

    public static List<Studio> toEntity(List<CsvMovie> csvMovieList) {
        return csvMovieList.stream()
                .flatMap(csvMovie -> stringToArray(csvMovie.getStudios()))
                .distinct()
                .map(studio -> Studio.builder().name(studio).build())
                .collect(Collectors.toList());
    }

    public static Stream<String> stringToArray(String studios) {
        return Arrays.stream(studios.split(", "));
    }
}
