package com.texoit.restgra.producer.repository;

import com.texoit.restgra.producer.model.Producer;
import com.texoit.restgra.producer.model.Winner;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProducerRepository extends CrudRepository<Producer, Long> {

    @Query("SELECT new com.texoit.restgra.producer.model.Winner(p.id, p.name, m.year) FROM #{#entityName} p JOIN p.movies m WHERE m.winner = 1 order by p.id, p.name, m.year")
    List<Winner> getWinners();
}
