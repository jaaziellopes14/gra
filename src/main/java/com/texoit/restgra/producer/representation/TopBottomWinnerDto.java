package com.texoit.restgra.producer.representation;

import com.texoit.restgra.producer.model.Winner;
import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
public class TopBottomWinnerDto {
    List<IntervalWinnerDto> min;
    List<IntervalWinnerDto> max;

    public static TopBottomWinnerDto toRepresentation(List<Winner> winners) {
        List<IntervalWinnerDto> minIntervalWinners = new ArrayList<>();
        List<IntervalWinnerDto> maxIntervalWinners = new ArrayList<>();
        Winner previousWinner = null;
        for (Winner winner : winners) {
            if (validWinners(previousWinner, winner)) {
                IntervalWinnerDto intervalWinner = IntervalWinnerDto.builder()
                        .producer(winner.getProducerName())
                        .previousWin(previousWinner.getYear())
                        .followingWin(winner.getYear()).build();
                verifyMinIntervals(minIntervalWinners, intervalWinner);
                verifyMaxIntervals(maxIntervalWinners, intervalWinner);
            }
            previousWinner = winner;
        }
        return TopBottomWinnerDto.builder().min(minIntervalWinners).max(maxIntervalWinners).build();
    }

    private static void verifyMinIntervals(List<IntervalWinnerDto> minIntervalWinners, IntervalWinnerDto intervalWinner) {
        if (minIntervalWinners.isEmpty()) {
            minIntervalWinners.add(intervalWinner);
            return;
        }
        Integer minInterval = minIntervalWinners.get(0).getInterval();
        if (intervalWinner.getInterval() > minInterval) {
            return;
        }
        if (intervalWinner.getInterval() < minInterval) {
            minIntervalWinners.clear();
        }
        minIntervalWinners.add(intervalWinner);
    }

    private static void verifyMaxIntervals(List<IntervalWinnerDto> maxIntervalWinners, IntervalWinnerDto intervalWinner) {
        if (maxIntervalWinners.isEmpty()) {
            maxIntervalWinners.add(intervalWinner);
            return;
        }
        Integer maxInterval = maxIntervalWinners.get(0).getInterval();
        if (intervalWinner.getInterval() < maxInterval) {
            return;
        }
        if (intervalWinner.getInterval() > maxInterval) {
            maxIntervalWinners.clear();
        }
        maxIntervalWinners.add(intervalWinner);
    }

    private static boolean validWinners(Winner previousWinner, Winner winner) {
        return previousWinner != null && winner.getProducerId().equals(previousWinner.getProducerId());
    }
}