package com.texoit.restgra.producer.representation;

import lombok.Builder;
import lombok.Getter;

@Getter
public class IntervalWinnerDto {
    private final String producer;
    private final Integer interval;
    private final Integer previousWin;
    private final Integer followingWin;

    @Builder
    public IntervalWinnerDto(String producer, Integer previousWin, Integer followingWin) {
        this.producer = producer;
        this.previousWin = previousWin;
        this.followingWin = followingWin;
        this.interval = followingWin - previousWin;
    }
}