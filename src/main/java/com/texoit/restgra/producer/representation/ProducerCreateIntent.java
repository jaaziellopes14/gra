package com.texoit.restgra.producer.representation;

import com.texoit.restgra.entreprise.CsvMovie;
import com.texoit.restgra.producer.model.Producer;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Getter
public class ProducerCreateIntent {

    private ProducerCreateIntent() {
    }

    public static List<Producer> toEntity(List<CsvMovie> csvMovieList) {
        return csvMovieList.stream()
                .flatMap(csvMovie -> stringToArray(csvMovie.getProducers()))
                .distinct()
                .map(studio -> Producer.builder().name(studio).build())
                .collect(Collectors.toList());
    }

    // TODO extrair para um adapter
    public static Stream<String> stringToArray(String producers) {
        return Arrays.stream(producers.split(", and |, | and "));
    }
}
