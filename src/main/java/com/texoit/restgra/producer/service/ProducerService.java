package com.texoit.restgra.producer.service;

import com.texoit.restgra.producer.model.Producer;
import com.texoit.restgra.producer.repository.ProducerRepository;
import com.texoit.restgra.producer.model.Winner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProducerService {

    private final ProducerRepository repository;

    public ProducerService(ProducerRepository repository) {
        this.repository = repository;
    }

    public Producer create(Producer toCreate) {
        return repository.save(toCreate);
    }

    public List<Winner> getWinners() {
        return repository.getWinners();
    }

}
