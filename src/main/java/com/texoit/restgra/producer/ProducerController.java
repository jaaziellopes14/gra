package com.texoit.restgra.producer;

import com.texoit.restgra.producer.representation.TopBottomWinnerDto;
import com.texoit.restgra.producer.model.Winner;
import com.texoit.restgra.producer.service.ProducerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/producers")
public class ProducerController {

	private final ProducerService findService;

	public ProducerController(ProducerService findService) {
		this.findService = findService;
	}

	@GetMapping("winners")
	public List<Winner> findAllWinners() {
		return findService.getWinners();
	}

	@GetMapping("top-bottom-interval-winner")
	public TopBottomWinnerDto findTopBottomIntervalWinners() {
		return TopBottomWinnerDto.toRepresentation(findService.getWinners());
	}
}