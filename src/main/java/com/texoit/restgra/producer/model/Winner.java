package com.texoit.restgra.producer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class Winner {
    private Long producerId;
    private String producerName;
    private Integer year;
}