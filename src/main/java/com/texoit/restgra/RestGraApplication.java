package com.texoit.restgra;

import com.texoit.restgra.entreprise.StartAppLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.IOException;

@SpringBootApplication
public class RestGraApplication {

	@Autowired
	private StartAppLoader service;

	public static void main(String[] args) {
		SpringApplication.run(RestGraApplication.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() throws IOException {
		service.loadCsv();
	}

}
