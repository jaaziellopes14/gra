package com.texoit.restgra.entreprise;

import com.opencsv.bean.CsvToBeanBuilder;
import com.texoit.restgra.movie.model.Movie;
import com.texoit.restgra.movie.wrapper.MovieWrapper;
import com.texoit.restgra.movie.service.MovieService;
import com.texoit.restgra.producer.model.Producer;
import com.texoit.restgra.producer.representation.ProducerCreateIntent;
import com.texoit.restgra.producer.service.ProducerService;
import com.texoit.restgra.studio.model.Studio;
import com.texoit.restgra.studio.wrapper.StudioWrapper;
import com.texoit.restgra.studio.service.StudioService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
public class StartAppLoader {

    private final StudioService studioService;
    private final ProducerService producerService;
    private final MovieService movieService;

    @Value("${datasource.archive}")
    private String archive;

    public StartAppLoader(StudioService studioService, ProducerService producerService, MovieService movieService) {
        this.studioService = studioService;
        this.producerService = producerService;
        this.movieService = movieService;
    }

    public void loadCsv() throws IOException {
        Reader reader = Files.newBufferedReader(Paths.get(archive));

        List<CsvMovie> csvMovieList = new CsvToBeanBuilder<CsvMovie>(reader)
                .withType(CsvMovie.class)
                .withSeparator(';')
                .withIgnoreLeadingWhiteSpace(true)
                .withIgnoreEmptyLine(true)
                .build().parse();
        List<Studio> studios = StudioWrapper.toEntity(csvMovieList);
        studios.forEach(studioService::create);

        List<Producer> producers = ProducerCreateIntent.toEntity(csvMovieList);
        producers.forEach(producerService::create);

        List<Movie> movies = MovieWrapper.toEntity(csvMovieList, studios, producers);
        movies.forEach(movieService::create);
    }
}
