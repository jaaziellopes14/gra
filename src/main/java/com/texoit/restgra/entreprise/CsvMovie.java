package com.texoit.restgra.entreprise;

import lombok.Data;

@Data
public class CsvMovie {

    private int year;
    private String title;
    private String studios;
    private String producers;
    private String winner;

}