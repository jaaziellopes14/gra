package com.texoit.restgra.producer;

import com.texoit.restgra.RestGraApplicationTests;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.nio.file.Files;
import java.nio.file.Paths;

class ProducerControllerTest extends RestGraApplicationTests {

    private MockMvc mockMvc;

    @Autowired
    private ProducerController controller;

    @Value("${datasource.json-result}")
    private String jsonResult;

    @BeforeEach
    public void setUp() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void findTopBottomIntervalWinnersWithSuccess() throws Exception {
        byte[] file = Files.readAllBytes(Paths.get(jsonResult));
        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/producers/top-bottom-interval-winner")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(new String(file)));
    }
}