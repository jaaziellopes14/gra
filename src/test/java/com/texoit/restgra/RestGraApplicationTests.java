package com.texoit.restgra;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:application.properties")
@SpringBootTest(classes = RestGraApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestGraApplicationTests {
}
